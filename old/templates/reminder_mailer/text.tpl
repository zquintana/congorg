Hello <%= name %>

You've been assigned to read <%= scripture %> during the Watchtower
Study of the week of <%= week %>. Thank you for your partipation. If
for some reason you are unable to complete this assignment, please
let me know as soon as possible.

Thank you,
Zach
