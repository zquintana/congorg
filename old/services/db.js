import DbPlugin from '../../src/lib/db'

export default function (bottle) {
  bottle.factory('db', function () {
    return DbPlugin.db
  })
}
