import Bottle from 'bottlejs'

let bottle = new Bottle()

const files = require.context('.', false, /\.js$/)

files.keys().forEach(key => {
  if (key === './index.js') return
  let callback = files(key).default

  if (typeof callback === 'function') {
    callback(bottle)
  }
})

const ContainerPlugin = {
  install (vue) {
    vue.prototype.$bottle = bottle
    vue.prototype.$container = bottle.container
  }
}

export default ContainerPlugin
