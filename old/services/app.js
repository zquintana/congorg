import Mail from '../mail/Mail'

export default function (bottle) {
  bottle.instanceFactory('mailer_mail', function (container) {
    return container.db.get('config').then(function (doc) {
      return new Mail(doc.email, doc.outbound_smtp)
    })
  })
  bottle.instanceFactory('mailer_reminder', function (container) {
    return container.mailer_mail.instance().then(function (mail) {
      const Mailer = require('../mail/ReminderMailer').default

      return new Mailer(mail)
    })
  })
}
