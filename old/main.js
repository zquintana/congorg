import Vue from 'vue'
import VueMaterial from 'vue-material'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'
import DbPlugin from '../src/lib/db'
import ContainerPlugin from './services'
import PasswordDialogPlugin from './util/PasswordDialogPlugin'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(DbPlugin)
Vue.use(VueMaterial)
Vue.use(ContainerPlugin)
Vue.use(PasswordDialogPlugin)
Vue.component('toolbar', require('@/components/Toolbar'))

/* eslint-disable no-new */
window.APP = new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
