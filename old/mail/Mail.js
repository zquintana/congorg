import nodemailer from 'nodemailer'

export default class Mail {
  constructor (email, host) {
    this.email = email
    this.host = host
  }

  setPassword (password) {
    this.password = password

    this.transport = nodemailer.createTransport({
      host: this.host,
      auth: {
        user: this.email,
        pass: this.password
      }
    })

    return this
  }

  verify () {
    return new Promise((resolve, reject) => {
      this.transport.verify((err, success) => {
        if (err) {
          reject(err, this)
        } else {
          resolve(success, this)
        }
      })
    })
  }
}
