
export default class ReminderMailer {
  constructor (mailTransport) {
    this.mailTransport = mailTransport
  }

  setPassword (password) {
    this.mailTransport.setPassword(password)

    return this
  }

  send (fromAddress) {
    let textTemplate = require('../templates/reminder_mailer/text.tpl')

    let mailOptions = {
      from: fromAddress, // sender address
      // to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
      subject: 'Hello ✔', // Subject line
      text: textTemplate(),
      html: '<b>Hello world ?</b>' // html body
    }

    return new Promise((resolve, reject) => {
      this.mailTransport.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log(error)
        }
      })
    })
  }
}
