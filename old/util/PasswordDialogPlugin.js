
function plugin (Vue) {
  if (plugin.installed) return
  plugin.installed = true

  Object.defineProperties(Vue.prototype, {
    $passwordDialog: {
      get () {
        let el = this
        while (el) {
          for (let i = 0; i < el.$children.length; i++) {
            const child = el.$children[i]
            if (child.$options._componentTag === 'password-dialog') {
              return child
            }
          }
          el = el.$parent
        }
        if (process.env.NODE_ENV !== 'production') {
          console.warn('Vue-alert component must be part of this component scope or any of the parents scope.')
        }
        return null
      }
    }
  })
}

export default plugin
