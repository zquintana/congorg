import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage')
    },
    {
      path: '/publishers',
      name: 'publishers',
      component: require('@/components/Publishers')
    },
    {
      path: '/publishers/add',
      name: 'publishers.add',
      component: require('@/components/Publishers/Form')
    },
    {
      path: '/publishers/:id/edit',
      name: 'publishers.edit',
      props: true,
      component: require('@/components/Publishers/Form')
    },
    {
      path: '/watchtower',
      name: 'watchtower',
      component: require('@/components/WatchtowerStudy')
    },
    {
      path: '/watchtower/:date/edit',
      name: 'watchtower.edit',
      props: true,
      component: require('@/components/WatchtowerStudy/Form')
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('@/components/Setting')
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
