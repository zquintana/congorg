import React from 'react'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import Reboot from 'material-ui/Reboot'
import Button from 'material-ui/Button'
import Menu, { MenuItem } from 'material-ui/Menu';
import { Link } from "react-router-dom"
import { css, StyleSheet } from 'aphrodite'
import { connect } from 'react-redux'

import {
    exportDatabase,
} from '@modules/settings/actions'
import ImportModal from '@modules/settings/views/ImportModal'

const styles = StyleSheet.create({
    title: {
        margin: '0 15px 0 0'
    }
})

const App = (props) => {
    return (
        <div>
            <Reboot/>
            {props.children}
        </div>
    )
}

class TopBarBase extends React.Component {
    state = {
        anchorEl: null,
    }

    handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget })
    }

    handleClose = () => {
        this.setState({ anchorEl: null })
    }

    onExportClick = () => {
        const { dispatch } = this.props
        dispatch(exportDatabase())

        this.handleClose()
    }

    onImportClick = () => {
        this.importModal.getWrappedInstance().handleOpen()
    }

    render() {
        const { anchorEl } = this.state
        const open = Boolean(anchorEl)

        return (
            <div>
                <AppBar position={'static'}>
                    <Toolbar>
                        <Typography type="title" color="inherit" className={css(styles.title)}>
                            Cong Org
                        </Typography>
                        <Button color="inherit" component={Link} to="/">
                            Publishers
                        </Button>
                        <Button color="inherit" component={Link} to="/watchtower_studies">
                            Watchtower Study
                        </Button>
                        <Button color="inherit" onClick={this.handleMenu}>
                            Tools
                        </Button>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={open}
                            onClose={this.handleClose}
                        >
                            <MenuItem onClick={this.onExportClick}>Export to File</MenuItem>
                            <MenuItem onClick={this.onImportClick}>Import from File</MenuItem>
                        </Menu>
                    </Toolbar>
                </AppBar>
                <ImportModal ref={instance => { this.importModal = instance }} />
            </div>
        )
    }
}

const TopBar = connect()(TopBarBase)

export {
    App,
    TopBar
}
