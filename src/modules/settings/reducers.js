
import {
    IMPORTED_DATABASE,
    IMPORT_ERROR,
    IMPORTING_DATABASE,
} from "./actions"

const INITIAL_STATE = {
    lastImport: null,
    lastError: null,
}
const dataImport = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case IMPORTING_DATABASE:
            return {
                ...state,
                lastError: null,
                lastImport: null,
            }

        case IMPORTED_DATABASE:
            return {
                ...state,
                lastImport: action.now,
                lastError: null,
            }

        case IMPORT_ERROR:
            return {
                ...state,
                lastError: action.error
            }

        default:
            return state
    }
}

const reducers = {
    dataImport,
}
export default reducers
