import MemoryStream from 'memorystream'
import FileSaver from 'file-saver'
import { db } from '@app/lib/db'
import {
    resetState,
} from "@app/etc/reducer"

export const EXPORTING_DATABASE = 'EXPORTING_DATABASE'
export const EXPORTED_DATABASE = 'EXPORTED_DATABASE'
export const IMPORTED_DATABASE = 'IMPORTED_DATABASE'
export const IMPORT_ERROR = 'IMPORT_ERROR'
export const IMPORTING_DATABASE = 'IMPORTING_DATABASE'

function importError(error) {
    return {
        type: IMPORT_ERROR,
        error
    }
}

function importingDatabase() {
    return {
        type: IMPORTING_DATABASE
    }
}

function importedDatabase() {
    const now = new Date()

    return {
        type: IMPORTED_DATABASE,
        now: now.getTime(),
    }
}

export function importDatabase(file) {
    return dispatch => {
        if (!file) {
            dispatch(importError('Missing file.'))

            return
        }

        dispatch(importingDatabase())
        const reader = new FileReader()
        reader.onerror = function (error) {
            dispatch(importError('Unable to import'))
        }
        reader.onload = function (evt) {
            if (reader.result.length < 1) {
                dispatch(importError('Import file is missing contents.'))

                return
            }

            db.load(reader.result)
            dispatch(importedDatabase())
        }
        reader.readAsText(file)
    }
}

function exportedDatabase() {
    return {
        type: EXPORTED_DATABASE
    }
}

function exportingDatabase() {
    return {
        type: EXPORTING_DATABASE,
    }
}

export function exportDatabase() {
    return dispatch => {
        dispatch(exportingDatabase())

        let exportData = ''
        const stream = new MemoryStream()
        stream.on('data', function(chunk) {
            exportData += chunk.toString()
        });

        db.dump(stream).then(() => {
            const data = new Blob([exportData], {type: "application/json;charset=utf-8"})
            FileSaver.saveAs(data, 'database.json')

            dispatch(exportedDatabase())
        })
    }
}