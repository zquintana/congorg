import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'material-ui/Modal'
import Input, { InputLabel} from 'material-ui/Input'
import Button from 'material-ui/Button'
import Snackbar from 'material-ui/Snackbar'
import { connect } from 'react-redux'
import { StyleSheet, css } from 'aphrodite'

import {
    importDatabase
} from "../actions"

const styles = StyleSheet.create({
    modal: {
        position: 'absolute',
        width: 300,
        backgroundColor: 'white',
        boxShadow: 5,
        padding: 15,
        top: `50%`,
        left: `50%`,
        transform: `translate(-50%, -50%)`,
    },
    buttons: {
        marginTop: 10
    }
})

class ImportModal extends React.Component {
    state = {
        open: false,
        files: null,
        showError: false,
        showSuccess: false,
    }

    constructor(props) {
        super(props)

        this.handleOpen  = this.handleOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            showError: nextProps.errorMessage && nextProps.errorMessage.length > 0,
            showSuccess: nextProps.imported,
        })

        if (nextProps.imported && this.state.open) {
            this.handleClose()
        }
    }

    handleOpen() {
        this.setState({ open: true })
    }

    handleClose() {
        this.setState({ open: false })
    }

    handleFileChange = () => event => {
        this.setState({
            files: event.target.files
        })
    }

    handleCloseAlert = () => (name, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ [name]: false });
    }

    handleImport = () => {
        const { dispatch } = this.props
        const file = this.state.files ? this.state.files[0] : null
        dispatch(importDatabase(file))
    }

    render() {
        return (
            <div>
                <Modal open={this.state.open}
                       onClose={this.handleClose}>
                    <div className={css(styles.modal)}>
                        <div>
                            <InputLabel>Choose file</InputLabel>
                            <Input type="file" onChange={this.handleFileChange()} />
                        </div>
                        <div className={css(styles.buttons)}>
                            <Button variant="raised" color="primary" onClick={this.handleImport}>
                                Import
                            </Button>
                        </div>
                    </div>
                </Modal>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.state.showError}
                    autoHideDuration={6000}
                    onClose={this.handleCloseAlert('showError')}
                    message={<span>{this.props.errorMessage}</span>} />
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.state.showSuccess}
                    autoHideDuration={6000}
                    onClose={this.handleCloseAlert('showSuccess')}
                    message={<span>Imported file.</span>} />
            </div>
        )
    }
}

ImportModal.propTypes = {
    errorMessage: PropTypes.string,
    imported: PropTypes.bool
}
ImportModal.defaultProps = {
    errorMessage: null,
    imported: false
}

const mapStateToProps = (state) => {
    const { dataImport } = state

    return {
        errorMessage: dataImport.lastError,
        imported: dataImport.lastImport && dataImport.lastImport > 0
    }
}

export default connect(mapStateToProps, null, null, { withRef: true })(ImportModal)
