import React from 'react'
import Typography from 'material-ui/Typography'
import { connect } from 'react-redux'
import Button from 'material-ui/Button'

import MainView from '@app/components/MainView'
import {
    exportDatabase,
} from "../actions"

export const SettingView = connect()((props) => {
    const { dispatch } = props
    const onExportClick = () => {
        dispatch(exportDatabase())
    }

    return (
        <MainView>
            <Typography type={'title'}>
                Settings
            </Typography>
            <div>
                <Button onClick={onExportClick}>
                    Export Database
                </Button>
            </div>
        </MainView>
    )
})
