
import {
    RECEIVE_PUBLISHERS,
    UPDATE_PUBLISHER_FORM,
    INITIAL_FORM_STATE,
    TOGGLE_PUBLISHER_FIELD,
    LOADED_PUBLISHER,
    SAVED_PUBLISHER,
    CLOSE_SAVED_SNACKBAR
} from "./actions"

const currentPublisher = (state = INITIAL_FORM_STATE, action) => {
    switch (action.type) {
        case SAVED_PUBLISHER:
            return action.savedPublisher

        case LOADED_PUBLISHER:
            return action.publisher

        case TOGGLE_PUBLISHER_FIELD:
            const value = state.privileges[action.value] ? state.privileges[action.value] : false

            return {
                ...state,
                privileges: {
                    [action.value]: !value
                }
            }

        case UPDATE_PUBLISHER_FORM:
            return Object.assign({}, state, {
                [action.field]: action.value
            })

        default:
            return state
    }
}

const publishers = (state = {
    records: [],
    isFetching: false,
    isSaving: false,
    showSaved: false
}, action) => {
    switch (action.type) {
        case CLOSE_SAVED_SNACKBAR:
            return Object.assign({}, state, {
                showSaved: false
            })
        case SAVED_PUBLISHER:
            return Object.assign({}, state, {
                showSaved: true
            })
        case RECEIVE_PUBLISHERS:
            return Object.assign({}, state, {
                records: action.publishers
            })

        default:
            return state
    }
}

const reducers = {
    publishers,
    currentPublisher
}

export default reducers
