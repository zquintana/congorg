import { getRepository } from "@app/lib/db"
import _ from 'underscore'

export const CHANGE_PUBLISHERS_REQUEST = 'CHANGE_PUBLISHERS_REQUEST'
export const REQUEST_PUBLISHERS = 'REQUEST_PUBLISHERS'
export const RECEIVE_PUBLISHERS = 'RECEIVE_PUBLISHERS'
export const SAVING_PUBLISHER = 'SAVING_PUBLISHER'
export const SAVED_PUBLISHER = 'SAVED_PUBLISHER'
export const LOADING_PUBLISHER = 'LOADING_PUBLISHER'
export const LOADED_PUBLISHER = 'LOADED_PUBLISHER'
export const UPDATE_PUBLISHER_FORM = 'UPDATE_PUBLISHER_FORM'
export const TOGGLE_PUBLISHER_FIELD = 'TOGGLE_PUBLISHER_FIELD'
export const CLOSE_SAVED_SNACKBAR = 'CLOSE_SAVED_SNACKBAR'

export const INITIAL_FORM_STATE = {
    gender: 'male',
    serving: 'unbaptized_publisher',
    email: '',
    firstName: '',
    lastName: '',
    privileges: {
        wtScriptureReader: false
    },
}

function loadingPublisher() {
    return {
        type: LOADING_PUBLISHER
    }
}

function loadedPublisher(publisher) {
    return {
        type: LOADED_PUBLISHER,
        publisher
    }
}

function savingPublisher() {
    return {
        type: SAVING_PUBLISHER
    }
}

function savedPublisher(savedPublisher) {
    return {
        type: SAVED_PUBLISHER,
        savedPublisher
    }
}

function requestPublishers() {
    return {
        type: REQUEST_PUBLISHERS,
    }
}

function receivePublishers(records) {
    return {
        type: RECEIVE_PUBLISHERS,
        publishers: records,
        received: true,
    }
}

function fetchPublishers(state) {
    return dispatch => {
        dispatch(requestPublishers())

        const repo = getRepository('Publisher')
        repo.all().then((docs) => {
            let unsorted = _.map(docs.rows, function (row) {
                return row.doc
            })

            const publishers = _.sortBy(unsorted, function (publisher) {
                return publisher.lastName + ', ' + publisher.firstName
            })

            dispatch(receivePublishers(publishers))
        })
    }
}

function shouldFetchPublishers(state) {
    const { publishers } = state;

    if (!publishers || !publishers.records || 1 > publishers.records.length) {
        return true;
    } else {
        return false;
    }
}

function updateType(field) {
    if ('privileges' === field) {
        return TOGGLE_PUBLISHER_FIELD
    } else {
        return UPDATE_PUBLISHER_FORM
    }
}

export function closeSavedSnackbar() {
    return {
        type: CLOSE_SAVED_SNACKBAR
    }
}

export function updatePublisherForm(field, value) {
    return {
        type: updateType(field),
        field,
        value
    }
}

export function loadPublisher(id) {
    return dispatch => {
        dispatch(loadingPublisher())

        if (id) {
            const repo = getRepository('Publisher')
            repo.find(id).then(response => {
                dispatch(loadedPublisher(response))
            })
        } else {
            dispatch(loadedPublisher(INITIAL_FORM_STATE))
        }
    }
}

export function savePublisher() {
    return (dispatch, getState) => {
        dispatch(savingPublisher())

        const repo = getRepository('Publisher')
        const state = getState()
        console.log(state.currentPublisher)

        repo.save(state.currentPublisher)
            .then((response) => {
                return repo.find(response.id)
            })
            .then((response) => {
                dispatch(savedPublisher(response))
            })
            .catch((error) => {
                console.log(error)
            })
    }
}

export function fetchPublishersIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchPublishers(getState())) {
            return dispatch(fetchPublishers(getState()))
        }
    }
}
