import React from 'react'
import { connect } from 'react-redux'
import Table, {
    TableHead,
    TableRow,
    TableBody,
    TableCell
} from 'material-ui/Table'

import PublisherRow from './PublisherRow'
import {
    fetchPublishersIfNeeded
} from "../actions"


class PublishersTable extends React.Component {
    componentDidMount() {
        const { dispatch } = this.props
        dispatch(fetchPublishersIfNeeded())
    }

    render() {
        const { publishers, isFetching } = this.props

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {publishers.map((publisher, i) => <PublisherRow publisher={publisher} key={publisher._id}/>)}
                    </TableBody>
                </Table>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const {
        isFetching,
        records
    } = state.publishers || {
        isFetching: false,
        records: []
    }

    return {
        isFetching,
        publishers: records
    }
}

export default connect(mapStateToProps)(PublishersTable)
