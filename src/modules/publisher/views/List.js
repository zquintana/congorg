import React from 'react'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import AddIcon from 'material-ui-icons/Add'
import { StyleSheet, css } from 'aphrodite'
import { Link } from 'react-router-dom'

import MainView from '@app/components/MainView'
import PublishersTable from './PublishersTable'

const styles = StyleSheet.create({
    buttons: {
        textAlign: 'right'
    }
})

const List = (props) => {
    return (
        <MainView>
            <Typography type={'title'}>
                Publishers
            </Typography>
            <PublishersTable/>
            <div className={css(styles.buttons)}>
                <Button fab color="primary" component={Link} to="/publishers/add">
                    <AddIcon/>
                </Button>
            </div>
        </MainView>
    )
}

export default List
