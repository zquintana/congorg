import React from 'react'
import TextField from 'material-ui/TextField'
import MenuItem from 'material-ui/Menu/MenuItem'
import Button from 'material-ui/Button'
import Save from 'material-ui-icons/Save'
import { connect } from 'react-redux'
import { FormControl, FormGroup, FormControlLabel, FormLabel } from 'material-ui/Form'
import Checkbox from 'material-ui/Checkbox'
import { StyleSheet, css } from 'aphrodite'
import Snackbar from 'material-ui/Snackbar'

import MainView from '@app/components/MainView'
import {
    savePublisher,
    updatePublisherForm,
    loadPublisher,
    closeSavedSnackbar
} from "../actions"

const GENDERS = [{
    value: 'male',
    label: 'Brother'
}, {
    value: 'female',
    label: 'Sister'
}]

const CONGREGATION_STATUS = [{
    value: 'unbaptized_publisher',
    label: 'Unbaptized Publisher'
},{
    value: 'publisher',
    label: 'Publisher'
},{
    value: 'ministerial_servant',
    label: 'Ministerial Servant'
},{
    value: 'elder',
    label: 'Elder'
}]

const styles = StyleSheet.create({
    formControl: {
        margin: 10,
    }
})

class Form extends React.Component {
    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ showSave: false });
    };


    componentDidMount() {
        const { match } = this.props
        this.props.loadPublisher(match.params.publisher)
    }

    render() {
        return (
            <MainView>
                <form noValidate autoComplete="off">
                    <FormControl className={css(styles.formControl)}>
                        <TextField id='firstName' label='First Name' onChange={this.props.onChange('firstName')} value={this.props.data.firstName} />
                    </FormControl>
                    <FormControl className={css(styles.formControl)}>
                        <TextField id='lastName' label='Last Name' onChange={this.props.onChange('lastName')} value={this.props.data.lastName} />
                    </FormControl>
                    <FormControl className={css(styles.formControl)}>
                        <TextField id='email' label='Email' onChange={this.props.onChange('email')} value={this.props.data.email} />
                    </FormControl>
                    <FormControl className={css(styles.formControl)}>
                        <TextField id='gender' select label='Gender' onChange={this.props.onChange('gender')} value={this.props.data.gender}>
                            {GENDERS.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </FormControl>
                    <FormControl className={css(styles.formControl)}>
                        <TextField id='serving' select label='Status' onChange={this.props.onChange('serving')} value={this.props.data.serving}>
                            {CONGREGATION_STATUS.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </FormControl>
                    <FormControl className={css(styles.formControl)}>
                        <FormLabel component="legend">Responsibilities</FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.props.data.privileges.wtScriptureReader}
                                        onChange={this.props.onChange('privileges')}
                                        value="wtScriptureReader"
                                    />
                                }
                                label="Watchtower Study Scripture Reader"
                            />
                        </FormGroup>
                    </FormControl>
                </form>
                <div>
                    <Button onClick={ this.props.history.goBack }>
                        Back
                    </Button>
                    <Button onClick={ this.props.onSave }>
                        <Save/>
                        Save
                    </Button>
                </div>

                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.props.showSaved}
                    autoHideDuration={6000}
                    onClose={this.props.onCloseSnackbar}
                    SnackbarContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">Publisher saved</span>}/>
            </MainView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.currentPublisher,
        showSaved: state.publishers.showSaved
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCloseSnackbar: () => {
            dispatch(closeSavedSnackbar())
        },
        onChange: name => event => {
            dispatch(updatePublisherForm(name, event.target.value))
        },
        onSave: () => {
            dispatch(savePublisher())
        },
        loadPublisher: publisher => {
            dispatch(loadPublisher(publisher))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)
