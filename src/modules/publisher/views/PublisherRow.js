import React from 'react'
import PropTypes from 'prop-types'
import {
    TableRow,
    TableCell
} from 'material-ui/Table'
import Button from 'material-ui/Button'
import Edit from 'material-ui-icons/Edit'
import { Link } from "react-router-dom"

const PublisherRow = (props) => {
    const { publisher } = props
    const id = publisher._id.substring(10)

    return (
        <TableRow>
            <TableCell>
                {`${publisher.firstName} ${publisher.lastName}`}
            </TableCell>
            <TableCell>
                <Button component={Link} to={`/publishers/${id}/edit`}>
                    <Edit/>
                </Button>
            </TableCell>
        </TableRow>
    )
}

PublisherRow.propTypes = {
    publisher: PropTypes.object.isRequired
}

export default PublisherRow
