import React from 'react'
import PropTypes from 'prop-types'
import {
    TableRow,
    TableCell
} from 'material-ui/Table'
import { Link } from "react-router-dom"
import Button from 'material-ui/Button'
import Edit from 'material-ui-icons/Edit'
import _ from 'underscore'

const StudyRow = (props) => {
    const numberOfScriptures = props.record ? _.size(props.record.scriptures) : 0

    return (
        <TableRow>
            <TableCell>
                {props.date.format('MMMM D Y')}
            </TableCell>
            <TableCell>
                {numberOfScriptures}
            </TableCell>
            <TableCell/>
            <TableCell>
                <Button component={Link} to={`/watchtower_studies/${props.date.format('YYYYMMDD')}`}>
                    <Edit/>
                </Button>
            </TableCell>
        </TableRow>
    )
}

StudyRow.propTypes = {
    date: PropTypes.object.isRequired,
    record: PropTypes.object
}

export default StudyRow