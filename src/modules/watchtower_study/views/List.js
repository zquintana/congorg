import React from 'react'
import PropTypes from 'prop-types'
import Typography from 'material-ui/Typography'
import Table, {
    TableHead,
    TableRow,
    TableBody,
    TableCell
} from 'material-ui/Table'
import { connect } from 'react-redux'
import moment from 'moment'
import _ from 'underscore'

import MainView from '@app/components/MainView'
import StudyRow from './StudyRow'
import {
    fetchStudiesIfNeeded
} from "../actions"

function dateRange(startDate) {
    let range = [startDate]

    for (let i = 1; i <= 8; i++) {
        range.push(moment(startDate).add(i, 'weeks'))
    }

    return range
}

class List extends React.Component {
    getRecord = (date) => {
        return _.findWhere(this.props.records, { date: date.format('YYYYMMDD') })
    }

    render() {
        const dates = dateRange(this.props.selectedDate)

        return (
            <MainView>
                <Typography type={'title'}>
                    Watchtower Studies
                </Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Week Of</TableCell>
                            <TableCell># Scriptures</TableCell>
                            <TableCell>Assignments Completed</TableCell>
                            <TableCell/>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {dates.map((date, i) => <StudyRow date={date} record={this.getRecord(date)} key={date} />)}
                    </TableBody>
                </Table>
            </MainView>
        )
    }
}

List.propTypes = {
    selectedDate: PropTypes.object.isRequired, // moment instance
    records: PropTypes.array
}
List.defaultProps = {
    records: [],
}

class ConnectedList extends List {
    componentDidMount() {
        const { dispatch } = this.props
        dispatch(fetchStudiesIfNeeded())
    }
}

const mapStateToProps = (state) => {
    return {
        selectedDate: state.watchtowerStudy.selectedDate,
        records: state.watchtowerStudy.records,
    }
}

export default connect(mapStateToProps)(ConnectedList)
