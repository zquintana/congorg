import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import { FormControl } from 'material-ui/Form'
import MenuItem from 'material-ui/Menu/MenuItem'
import { connect } from 'react-redux'
import {
    TableRow,
    TableCell
} from 'material-ui/Table'
import { css } from 'aphrodite'

import { styles } from "./Form"
import {
    updateReader,
} from '../actions'

const ScriptureRow = (props) => {
    const { dispatch, index } = props
    const onChange = () => event => {
        dispatch(updateReader(index, event.target.value))
    }

    return (
        <TableRow>
            <TableCell>
                {props.assignment.scripture}
            </TableCell>
            <TableCell>
                <FormControl className={css(styles.formControl)}>
                    <TextField select onChange={onChange()} value={props.assignment.reader}>
                        {props.readers.map(reader => (
                            <MenuItem key={reader._id} value={reader._id}>
                                {`${reader.firstName} ${reader.lastName}`}
                            </MenuItem>
                        ))}
                    </TextField>
                </FormControl>
            </TableCell>
        </TableRow>
    )
}

ScriptureRow.propTypes = {
    assignment: PropTypes.object.isRequired,
    readers: PropTypes.array,
    index: PropTypes.number.isRequired,
}
ScriptureRow.defaultProps = {
    readers: [],
}

export default connect()(ScriptureRow)