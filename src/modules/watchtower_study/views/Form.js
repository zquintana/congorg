import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import { FormControl, FormGroup, FormControlLabel, FormLabel } from 'material-ui/Form'
import { StyleSheet, css } from 'aphrodite'
import Button from 'material-ui/Button'
import Table, {
    TableHead,
    TableRow,
    TableBody,
    TableCell
} from 'material-ui/Table'
import Checkbox from 'material-ui/Checkbox'
import Snackbar from 'material-ui/Snackbar'
import { connect } from 'react-redux'

import MainView from '@app/components/MainView'
import ScriptureRow from './ScriptureRow'
import {
    loadStudy,
    saveBulk,
} from "../actions"

export const styles = StyleSheet.create({
    formControl: {
        margin: 10,
    }
})

class Form extends React.Component {
    state = {
        scriptures: '',
        autoFill: true,
        showSaved: false,
    }

    handleCloseSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
    }

    handleScriptureChange = () => event => {
        this.setState({
            scriptures: event.target.value
        })
    }

    setAutoFill = () => event => {
        this.setState({
            autoFill: event.target.checked
        })
    }

    render() {
        const scriptures = this.props.scriptures.split('\n')
        const showScriptures = this.props.record.scriptures.length > 0

        return (
            <MainView>
                <form noValidate autoComplete="off">
                    {this.props.showBulk &&
                        <div>
                            <FormControl className={css(styles.formControl)}>
                                <TextField
                                    id="scriptures"
                                    label="Scriptures (One Per Line)"
                                    multiline
                                    rowsMax="6"
                                    value={this.state.scriptures}
                                    onChange={this.handleScriptureChange()}
                                    margin="normal"
                                />
                            </FormControl>
                            <FormControl className={css(styles.formControl)}>
                                <FormGroup>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={this.state.autoFill}
                                                onChange={this.setAutoFill()}
                                                value="autoFill"
                                            />
                                        }
                                        label="Auto fill assignments?"
                                    />
                                </FormGroup>
                            </FormControl>
                            <div>
                                <Button onClick={this.props.onAddClick(this.props.record, this.state.scriptures, this.state.autoFill)}>
                                    Add
                                </Button>
                            </div>
                        </div>
                    }
                    {showScriptures &&
                        <div>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Scripture</TableCell>
                                        <TableCell>Assigned To</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.props.record.scriptures.map((scripture, i) => <ScriptureRow assignment={scripture} readers={this.props.readers} key={i} index={i} />)}
                                </TableBody>
                            </Table>
                        </div>
                    }
                </form>
                <div>
                    <Button onClick={ this.props.history.goBack }>
                        Back
                    </Button>
                </div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.state.showSaved}
                    autoHideDuration={6000}
                    onClose={this.handleCloseSnackbar}
                    SnackbarContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">Study saved</span>}/>
            </MainView>
        )
    }
}

Form.propTypes = {
    showBulk: PropTypes.bool,
    date: PropTypes.string,
    readers: PropTypes.array,
    record: PropTypes.object.isRequired,
    onAddClick: PropTypes.func,
}
Form.defaultProps = {
    showBulk: true,
    scriptures: '',
    readers: [],
    onAddClick: function () {},
}

class ConnectedForm extends Form {
    componentWillMount() {
        const { match, dispatch } = this.props
        dispatch(loadStudy(match.params.date))
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.watchtowerStudyForm,
        readers: state.watchtowerStudy.readers
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        onAddClick: (record, scriptures, autoFill) => () => {
            dispatch(saveBulk(record, scriptures, autoFill))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectedForm)
