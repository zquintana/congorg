import moment from "moment/moment"

import {
    RECEIVE_WATCHTOWER_STUDIES,
    RECEIVE_WATCHTOWER_STUDY,
    NEW_WATCHTOWER_STUDY,
    LOADED_WATCHTOWER_STUDY_READERS,
    UPDATE_WATCHTOWER_STUDY,
    UPDATE_WATCHTOWER_STUDY_READER,
} from './actions'

function normalizeDate (momentDate) {
    if (momentDate.day() === 1) {
        return momentDate
    }

    if (momentDate.day() === 0) {
        return momentDate.add(1, 'day')
    }

    // everything else
    const diff = momentDate.day() - 1

    return momentDate.subtract(diff, 'days')
}

const INITIAL_STATE = {
    selectedDate: normalizeDate(moment().subtract('2', 'weeks')),
    records: [],
    readers: [],
}

const watchtowerStudy = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOADED_WATCHTOWER_STUDY_READERS:
            return Object.assign({}, state, {
                readers: action.readers,
            })

        case RECEIVE_WATCHTOWER_STUDIES:
            return Object.assign({}, state, {
                records: action.records,
            })

        default:
            return state
    }
}

const watchtowerStudyForm = (state = {
    record: {
        showBulk: false,
        scriptures: []
    },
}, action) => {
    switch (action.type) {
        case UPDATE_WATCHTOWER_STUDY_READER:
            let record = state.record
            record.scriptures[action.index].reader = action.newReader

            return {
                ...state,
                record
            }

        case UPDATE_WATCHTOWER_STUDY:
            return {
                ...state,
                record: action.study
            }

        case RECEIVE_WATCHTOWER_STUDY:
            return {
                ...state,
                record: action.study,
                showBulk: false
            }

        case NEW_WATCHTOWER_STUDY:
            return {
                ...state,
                record: action.study,
                showBulk: true
            }


        default:
            return state
    }
}

const reducers = {
    watchtowerStudy,
    watchtowerStudyForm
}

export default reducers
