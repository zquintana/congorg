import { getRepository, db } from "@app/lib/db"
import _ from "underscore";

export const REQUEST_WATCHTOWER_STUDIES = 'REQUEST_WATCHTOWER_STUDIES'
export const RECEIVE_WATCHTOWER_STUDIES = 'RECEIVE_WATCHTOWER_STUDIES'
export const REQUEST_WATCHTOWER_STUDY = 'REQUEST_WATCHTOWER_STUDY'
export const RECEIVE_WATCHTOWER_STUDY = 'RECEIVE_WATCHTOWER_STUDY'
export const NEW_WATCHTOWER_STUDY = 'NEW_WATCHTOWER_STUDY'
export const LOADED_WATCHTOWER_STUDY_READERS = 'LOADED_WATCHTOWER_STUDY_READERS'
export const SAVING_BULK_READERS = 'SAVING_BULK_READERS'
export const UPDATE_WATCHTOWER_STUDY = 'UPDATE_WATCHTOWER_STUDY'
export const UPDATE_WATCHTOWER_STUDY_READER = 'UPDATE_WATCHTOWER_STUDY_READER'


function requestStudy(date) {
    return {
        type: REQUEST_WATCHTOWER_STUDY,
        date
    }
}

function receiveStudy(study) {
    return {
        type: RECEIVE_WATCHTOWER_STUDY,
        study
    }
}

function newStudy(date) {
    return {
        type: NEW_WATCHTOWER_STUDY,
        study: {
            date: date,
            scriptures: [],
        }
    }
}
function loadedReaders(readers) {
    return {
        type: LOADED_WATCHTOWER_STUDY_READERS,
        readers
    }
}

function loadReaders() {
    return dispatch => {
        const repo = getRepository('Publisher')

        repo.allReaders().then((results) => {
            dispatch(loadedReaders(results))
        })
    }
}

function savingBulkReaders() {
    return {
        type: SAVING_BULK_READERS,
    }
}

export function saveStudy() {
    return (dispatch, getState) => {
        const state = getState()
        const record = state.watchtowerStudyForm.record
        const readerIds = _.map(record.scriptures, function (entry) {
            return entry.reader
        })
        const readers = _.filter(state.watchtowerStudy.readers, function (reader) {
            return readerIds.indexOf(reader._id) >= 0
        })

        readers.forEach((reader) => {
            reader.history.lastWatchtowerScriptureRead = record.date
        })
        db.bulkDocs(readers).then(() => {
            dispatch(loadReaders())
        })

        const repo = getRepository('WatchtowerStudy')
        repo.save(record)
        .then((result) => {
            return repo.find(result.id)
        })
        .then((result) => {
            dispatch(receiveStudy(result))
        })
    }
}

function updateStudyReader(index, newReader) {
    return {
        type: UPDATE_WATCHTOWER_STUDY_READER,
        index,
        newReader
    }
}

export function updateReader(index, newReader) {
    return dispatch => {
        dispatch(updateStudyReader(index, newReader))
        dispatch(saveStudy())
    }
}

function updateStudy(study) {
    return {
        type: UPDATE_WATCHTOWER_STUDY,
        study
    }
}

export function saveBulk(record, scripturesText, autoFill = true) {
    return (dispatch, getState) => {
        dispatch(savingBulkReaders())
        const scriptures = scripturesText.split('\n')
        const state = getState()

        scriptures.forEach((scripture, index) => {
            if (autoFill) {
                let reader = state.watchtowerStudy.readers[index]

                record.scriptures.push({
                    scripture: scripture,
                    reader: reader._id
                })
            }
        })

        dispatch(updateStudy(record))
        dispatch(saveStudy())
    }
}

export function loadStudy(date) {
    return (dispatch) => {
        dispatch(requestStudy(date))
        dispatch(loadReaders())

        const repo = getRepository('WatchtowerStudy')
        repo.find(date).then(result => {
            dispatch(receiveStudy(result))
        }).catch(error => {
            if (error.status === 404) {
                dispatch(newStudy(date))
            }
        })
    }
}

function requestStudies() {
    return {
        type: REQUEST_WATCHTOWER_STUDIES
    }
}

function receiveStudies(studies) {
    return {
        type: RECEIVE_WATCHTOWER_STUDIES,
        records: studies
    }
}

function fetchStudies(state) {
    return dispatch => {
        dispatch(requestStudies())

        const repo = getRepository('WatchtowerStudy')
        repo.allStartingWith(state.watchtowerStudy.selectedDate).then((results) => {
            dispatch(receiveStudies(_.pluck(results.rows, 'doc')))
        })
    }
}

function shouldFetchStudies(state) {
    const { watchtowerStudy } = state;

    if (!watchtowerStudy || !watchtowerStudy.records || 1 > watchtowerStudy.records.length) {
        return true;
    } else {
        return false;
    }
}

export function fetchStudiesIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchStudies(getState())) {
            return dispatch(fetchStudies(getState()))
        }
    }
}