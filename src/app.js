import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route, Switch } from 'react-router'
import { BrowserRouter as Router } from 'react-router-dom'
import Grid from 'material-ui/Grid'

import { configureStore } from './etc/store'
import { App, TopBar } from './modules/core/views'
import * as PublisherList from './modules/publisher/views/List'
import * as PublisherForm from './modules/publisher/views/Form'
import * as WatchtowerList from './modules/watchtower_study/views/List'
import * as WatchtowerForm from './modules/watchtower_study/views/Form'

const store = configureStore()

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App>
                <TopBar/>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Route exact path="/" component={PublisherList.default}/>
                        <Route path="/publishers/add" component={PublisherForm.default}/>
                        <Route path="/publishers/:publisher/edit" component={PublisherForm.default}/>
                        <Route exact path="/watchtower_studies" component={WatchtowerList.default} />
                        <Route path="/watchtower_studies/:date" component={WatchtowerForm.default} />
                    </Grid>
                </Grid>
            </App>
        </Router>
    </Provider>,
    document.getElementById('root')
)
