import React from 'react'
import Paper from 'material-ui/Paper'
import { StyleSheet, css } from 'aphrodite'

const styles = StyleSheet.create({
    mainContainer: {
        padding: 15
    }
})

const MainView = (props) => {
    const { children } = props
    const className = `${css(styles.mainContainer)} ${props.className}`
    delete props[children]
    delete props[className]

    return (
        <Paper className={className} {...props}>
            {children}
        </Paper>
    )
}

export default MainView
