import shortid from 'shortid'

export default class AbstractRepository {
  constructor (db, type) {
    this.db = db
    this.type = type
  }

  all () {
    return this.db.allDocs({
      startkey: this.type + '_',
      endkey: this.type + '_\uffff',
      include_docs: true
    })
  }

  find (id) {
    if (id.indexOf('_') < 0) {
      id = this.formatId(id)
    }

    return this.db.get(id)
  }

  formatId (id) {
    return this.type + '_' + id
  }

  generateId () {
    return this.formatId(shortid.generate())
  }

  save (doc) {
    doc.type = this.type
    if (!doc._id) {
      doc._id = this.generateId(doc)
    }

    if (this.saveTimeout) {
      clearTimeout(this.saveTimeout)
    }

    return this.db.put(doc)
  }

  saveDelayed (doc) {
    if (this.saveTimeout) {
      clearTimeout(this.saveTimeout)
    }

    return new Promise((resolve) => {
      this.saveTimeout = setTimeout(() => {
        this.save(doc).then((response) => {
          resolve(response, this)
        })
      }, 500)
    })
  }
}
