import PouchDB from 'pouchdb-browser'
import repositories from './repository/index'
import replicationStream from 'pouchdb-replication-stream/dist/pouchdb.replication-stream'
import load from 'pouchdb-load'

PouchDB.plugin(replicationStream.plugin)
PouchDB.plugin(load)
PouchDB.adapter('writableStream', replicationStream.adapters.writableStream)

const db = new PouchDB('./storage/congo.db')
let repoInstances = []

const getRepository = (name) => {
    const repoName = `${name}Repository`
    if (!repoInstances[name]) {
        repoInstances[repoName] = new repositories[repoName](db)
    }

    return repoInstances[repoName]
}

export {
    getRepository,
    db
}
