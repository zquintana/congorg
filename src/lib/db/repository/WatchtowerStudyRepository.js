import Repository from '../AbstractRepository'
import moment from 'moment'

const DATE_FORMAT = 'YYYYMMDD'

export default class WatchtowerStudyRepository extends Repository {
  constructor (db) {
    super(db, 'watchtower')
  }

  generateId (doc) {
    return this.formatId(moment(doc.date).format(DATE_FORMAT))
  }

  allStartingWith (date, limit = 10) {
    return this.db.allDocs({
      startkey: this.type + '_' + moment(date).format(DATE_FORMAT),
      endkey: this.type + '_\uffff',
      include_docs: true
    })
  }
}
