import Repository from '../AbstractRepository'

export default class extends Repository {
  constructor (db) {
    super(db, 'territory')
  }
}
