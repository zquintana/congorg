import _ from 'underscore'
import moment from 'moment'
import Repository from '../AbstractRepository'

export default class PublisherRepository extends Repository {
  constructor (db) {
    super(db, 'publisher')
  }

  allReaders () {
    return this.all().then((results) => {
      let publishers = _.sortBy(results.rows.map(function (result) {
        return result.doc
      }), function (publisher) {
        if (!publisher.history) {
          publisher.history = {
            lastWatchtowerScriptureRead: 0
          }
        }

        return moment(publisher.history.lastWatchtowerScriptureRead).unix()
      })

      return new Promise(function (resolve, reject) {
        resolve(publishers)
      })
    })
  }
}
