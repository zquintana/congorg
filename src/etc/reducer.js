import { combineReducers } from "redux"
import { routerReducer } from "react-router-redux"
import modules from "@app/etc/modules"

export const RESET_STATE = 'RESET_STATE'

export function resetState() {
    return {
        type: RESET_STATE
    }
}

export default function () {
    const reducers = combineReducers({
        ...modules,
        routing: routerReducer
    })
    const initialState = reducers({}, {})

    return (state, action) => {
        if (action.type === RESET_STATE) {
            return initialState
        }

        return reducers(state, action)
    }
}