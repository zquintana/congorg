import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducer from './reducer'

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const configureStore = (preloadedState = {}) => {
    const middlewares = [thunkMiddleware]
    const enhancers = composeEnhancers(
        applyMiddleware(...middlewares)
    )

    return createStore(reducer(), preloadedState, enhancers)
}

export {
    configureStore
}
