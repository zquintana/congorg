import publisher from '@modules/publisher/reducers'
import watchtowerStudy from '@modules/watchtower_study/reducers'
import settings from '@modules/settings/reducers'

export default {
    ...publisher,
    ...watchtowerStudy,
    ...settings,
}
