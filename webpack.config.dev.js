const path = require('path');

module.exports = require('./webpack.config')({
    environment: "dev",
    dev_server: {
        contentBase: path.join(__dirname, 'web'),
        compress: true,
        port: 9000
    }
});
